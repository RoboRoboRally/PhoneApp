﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChoicePage : ContentPage
    {
        public ChoicePage(TaskCompletionSource<CardInfo> tcs, UpgradeCardInfo card)
        {
            BindingContext = new ChoiceViewModel(tcs, card);
            InitializeComponent();

            choiceLabel.Text = AppResources.Choice;

            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var upgrade in ((ChoiceViewModel)BindingContext).UpgradeCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("UpgradeCards[" + c + "].Card.CardId", BindingMode.OneWay, new ChoiceConverter(), card.CardId));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("UpgradeCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("UpgradeCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }
            bigImage.SetBinding(Image.SourceProperty, new Binding("SelectedCard.Card.CardId", BindingMode.OneWay, new ChoiceConverter(), card.CardId));

            // Disable arrow buttons if there are not enough cards
            if (((ChoiceViewModel)BindingContext).UpgradeCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = Constants.dimOpacity;
                arrows.nextImage.Opacity = Constants.dimOpacity;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}