﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp
{
    /// <summary>
    /// Code-behind for programming page
    /// This page is displayed during the programming phase
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class CardsProgrammingPage : ContentPage
    {
        public CardsProgrammingPage(TaskCompletionSource<CardInfo[]> tcs)
        {
            BindingContext = new CardsProgrammingViewModel(tcs);
            InitializeComponent();
            Register_1.Text = AppResources.Register + " 1";
            Register_2.Text = AppResources.Register + " 2";
            Register_3.Text = AppResources.Register + " 3";
            Register_4.Text = AppResources.Register + " 4";
            Register_5.Text = AppResources.Register + " 5";

            CardGrid.ColumnDefinitions = new ColumnDefinitionCollection { };

            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var card in ((CardsProgrammingViewModel)BindingContext).OfferedCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("OfferedCards[" + c + "].Card.CardId", BindingMode.OneWay, new ProgrammingCardConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("OfferedCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("OfferedCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }

            // Disable arrow buttons if there are not enough cards
            if (((CardsProgrammingViewModel)BindingContext).OfferedCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = Constants.dimOpacity;
                arrows.nextImage.Opacity = Constants.dimOpacity;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}