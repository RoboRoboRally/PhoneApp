﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    /// <summary>
    /// Code-behind for discard page
    /// This page is displayed when the player is offered an option to discard an upgrade in order to free space in inventory    
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class DiscardPage : ContentPage
    {
        public DiscardPage(TaskCompletionSource<CardInfo> tcs, ObservableCollection<UpgradeCardInfo> upgrades)
        {
            BindingContext = new DiscardViewModel(tcs, upgrades);
            InitializeComponent();

            discardLabel.Text = AppResources.Discard;

            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var upgrade in ((DiscardViewModel)BindingContext).UpgradeCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("UpgradeCards[" + c + "].Card.CardId", BindingMode.OneWay, new UpgradeConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("UpgradeCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("UpgradeCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }

            // Disable arrow buttons if there are not enough cards
            if (((DiscardViewModel)BindingContext).UpgradeCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = Constants.dimOpacity;
                arrows.nextImage.Opacity = Constants.dimOpacity;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Back();
            return true;
        }
    }
}