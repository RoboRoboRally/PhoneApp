﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChooseOnePage : ContentPage
    {
        public ChooseOnePage(TaskCompletionSource<CardInfo[]> tcs)
        {
            BindingContext = new ChooseOneViewModel(tcs);
            InitializeComponent();

            chooseOneLabel.Text = AppResources.ChooseOneCard;

            CardGrid.ColumnDefinitions = new ColumnDefinitionCollection { };

            int c = 0;
            foreach (var card in ((ChooseOneViewModel)BindingContext).OfferedCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("OfferedCards[" + c + "].Card.CardId", BindingMode.OneWay, new ProgrammingCardConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("OfferedCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("OfferedCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }

            // Disable arrow buttons if there are not enough cards
            if (((ChooseOneViewModel)BindingContext).OfferedCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = Constants.dimOpacity;
                arrows.nextImage.Opacity = Constants.dimOpacity;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}