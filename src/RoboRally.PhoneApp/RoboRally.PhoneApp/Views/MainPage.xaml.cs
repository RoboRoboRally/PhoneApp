﻿using System;
using Xamarin.Forms;

namespace RoboRally.PhoneApp
{
    public partial class MainPage : ContentPage
    {
        public MainPage()
        {
            NavigationPage.SetHasNavigationBar(this, false);
            InitializeComponent();
        }

        async void OnImageNameTapped(object sender, EventArgs args)
        {
            await Navigation.PushModalAsync(new IntroPage());
        }

    }
}
