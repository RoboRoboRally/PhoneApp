﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    /// <summary>
    /// Code-behind for activation page
    /// This page is displayed during the activation phase    
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ActivationPage : ContentPage
    {
        public ActivationPage()
        {
            BindingContext = new ActivationViewModel();
            InitializeComponent();
            Register_1.Text = AppResources.Register + " 1";
            Register_2.Text = AppResources.Register + " 2";
            Register_3.Text = AppResources.Register + " 3";
            Register_4.Text = AppResources.Register + " 4";
            Register_5.Text = AppResources.Register + " 5";

            CardGrid.ColumnDefinitions = new ColumnDefinitionCollection { };
            
            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var card in ((ActivationViewModel)BindingContext).UpgradeCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("UpgradeCards[" + c + "].Card.CardId", BindingMode.OneWay, new UpgradeConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("UpgradeCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("UpgradeCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }
            
            // Disable arrow buttons if there are not enough cards
            if (((ActivationViewModel)BindingContext).UpgradeCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = 0.5;
                arrows.nextImage.Opacity = 0.5;
            }

            // Disable send if there are no cards
            if (((ActivationViewModel)BindingContext).UpgradeCards.Count == 0)
            {
                activationLabel.Text = AppResources.NoTemporary;
                ((ActivationViewModel)BindingContext).SendEnabled = false;
                ((ActivationViewModel)BindingContext).SendOpacity = 0.5;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}