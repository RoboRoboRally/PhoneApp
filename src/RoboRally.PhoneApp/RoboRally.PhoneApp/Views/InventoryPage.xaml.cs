﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class InventoryPage : ContentPage
    {
        public InventoryPage()
        {
            BindingContext = new InventoryViewModel();
            InitializeComponent();

            inventoryLabel.Text = AppResources.Inventory;

            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var card in ((InventoryViewModel)BindingContext).UpgradeCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("UpgradeCards[" + c + "].Card.CardId", BindingMode.OneWay, new UpgradeConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("UpgradeCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("UpgradeCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }

            // Disable arrow buttons if there are not enough cards
            if (((InventoryViewModel)BindingContext).UpgradeCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = Constants.dimOpacity;
                arrows.nextImage.Opacity = Constants.dimOpacity;
            }

            if (((InventoryViewModel)BindingContext).UpgradeCards.Count == 0)
            {
                cardsLabel.Text = AppResources.NoUpgrades;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Back();
            return true;
        }
    }
}