﻿using RoboRally.PhoneApp.Models;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp
{
    /// <summary>
    /// Page which displays information about the project
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class AboutPage : ContentPage
    {
        /// <summary>
        /// Loads text about the project
        /// Some of the text was too large to fit into AppResource
        /// Thus special file in the Texts folder is loaded and used
        /// </summary>
        public AboutPage()
        {
            InitializeComponent();
            AboutProjectHeadline.Text = AppResources.AboutProjectHeadline;
            AboutProjectText.Text = TextLoader.LoadText(AppResources.AboutProjectText);
            AboutTeamHeadline.Text = AppResources.AboutTeamHeadline;
            AboutTeamText.Text = TextLoader.LoadText(AppResources.AboutTeamText);
        }
    }
}