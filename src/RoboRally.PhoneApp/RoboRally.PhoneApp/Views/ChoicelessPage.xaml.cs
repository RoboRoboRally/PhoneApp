﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ChoicelessPage : ContentPage
    {
        public ChoicelessPage(TaskCompletionSource<bool> tcs, CardInfo card)
        {
            BindingContext = new ChoicelessViewModel(tcs, card);
            InitializeComponent();

            choicelessLabel.Text = AppResources.Choiceless;

            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var upgrade in ((ChoicelessViewModel)BindingContext).UpgradeCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("UpgradeCards[" + c + "].Card.CardId", BindingMode.OneWay, new UpgradeConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("UpgradeCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("UpgradeCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}