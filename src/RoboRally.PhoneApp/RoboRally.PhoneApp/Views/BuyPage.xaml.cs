﻿using RoboRally.PhoneApp.Converters;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Server.Interfaces;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    /// <summary>
    /// Code-behind for buy page
    /// This page is displayed during the buying phase
    /// </summary>
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class BuyPage : ContentPage
    {
        public BuyPage(TaskCompletionSource<PurchaseResponse> tcs, PurchaseOffer offer)
        {
            BindingContext = new BuyViewModel(tcs, offer);
            InitializeComponent();

            buyLabel.Text = AppResources.Buy;

            // Fill the top line grid with cards and bind them
            int c = 0;
            foreach (var card in ((BuyViewModel)BindingContext).UpgradeCards)
            {
                Image cardImage = new Image();
                cardImage.SetBinding(Image.SourceProperty, new Binding("UpgradeCards[" + c + "].Card.CardId", BindingMode.OneWay, new UpgradeConverter()));
                cardImage.SetBinding(Image.BackgroundColorProperty, new Binding("UpgradeCards[" + c + "].Color"));
                cardImage.SetBinding(Image.OpacityProperty, new Binding("UpgradeCards[" + c + "].Opacity"));
                CardGrid.Children.Add(cardImage, c, 0);
                c++;
            }

            // Disable arrow buttons if there are not enough cards
            if (((BuyViewModel)BindingContext).UpgradeCards.Count < 2)
            {
                arrows.previousButton.IsEnabled = false;
                arrows.nextButton.IsEnabled = false;
                arrows.previousImage.Opacity = Constants.dimOpacity;
                arrows.nextImage.Opacity = Constants.dimOpacity;
            }
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}