﻿using RoboRally.PhoneApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class GameOverPage : ContentPage
    {
        public GameOverPage()
        {
            InitializeComponent();
            GameOverLabel.Text = AppResources.GameOver;
        }

        /// <summary>
        /// Overrides the functionality of mechanical or semi-mechanical button of a phone
        /// Triggers general Quit game dialog
        /// </summary>
        /// <returns></returns>
        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}