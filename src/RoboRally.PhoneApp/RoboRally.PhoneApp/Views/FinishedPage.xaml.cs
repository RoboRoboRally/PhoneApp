﻿using RoboRally.PhoneApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class FinishedPage : ContentPage
    {
        public FinishedPage()
        {
            InitializeComponent();
            FinishedLabel.Text = AppResources.Finished;
        }

        protected override bool OnBackButtonPressed()
        {
            Controller.Quit();
            return true;
        }
    }
}