﻿using RoboRally.PhoneApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class IntroPage : ContentPage
    {
        public IntroPage()
        {
            BindingContext = new IntroViewModel();
            InitializeComponent();

            Play.Text = AppResources.Play;
            Rules.Text = AppResources.Rules;
            About.Text = AppResources.About;
        }
    }
}