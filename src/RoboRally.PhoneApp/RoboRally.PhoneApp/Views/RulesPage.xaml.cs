﻿using RoboRally.PhoneApp.Models;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RulesPage : ContentPage
    {
        public RulesPage()
        {
            InitializeComponent();
            RulesText.Text = TextLoader.LoadText(AppResources.RulesText);
        }
    }
}