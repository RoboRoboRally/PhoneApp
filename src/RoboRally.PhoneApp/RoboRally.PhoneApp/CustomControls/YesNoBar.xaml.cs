﻿using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class YesNoBar : Xamarin.Forms.Grid
    {
        public YesNoBar()
        {
            InitializeComponent();
        }
    }
}