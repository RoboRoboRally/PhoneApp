﻿using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ArrowBar : Xamarin.Forms.Grid
    {
        public ArrowBar()
        {
            InitializeComponent();
        }
    }
}