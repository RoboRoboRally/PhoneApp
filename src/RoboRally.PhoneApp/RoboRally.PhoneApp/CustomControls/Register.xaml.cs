﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Model.Textures.CardTextures;
using System.Windows.Input;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class Register : Xamarin.Forms.Grid
    {
        CardTextureLoader textureLoader = new CardTextureLoader("RoboRally.PhoneApp/GameData/Textures/CardTextures");
        ColorTypeConverter converter = new ColorTypeConverter();

        public static readonly BindableProperty TextProperty = BindableProperty.Create(nameof(Text), typeof(string), typeof(Register), default(string), Xamarin.Forms.BindingMode.TwoWay);
        public string Text
        {
            get
            {
                return (string)GetValue(TextProperty);
            }

            set
            {
                SetValue(TextProperty, value);
            }
        }

        public static readonly BindableProperty BoxViewProperty = BindableProperty.Create(nameof(BoxView), typeof(Color), typeof(Register), default(Color), BindingMode.TwoWay);
        public Color BoxView
        {
            get
            {
                return (Color)GetValue(BoxViewProperty);
            }

            set
            {
                SetValue(BoxViewProperty, value);
            }
        }

        public static readonly BindableProperty ImageProperty = BindableProperty.Create(nameof(Image), typeof(string), typeof(Register), default(string), Xamarin.Forms.BindingMode.TwoWay);
        public string Image
        {
            get
            {
                return (string)GetValue(ImageProperty);
            }

            set
            {
                SetValue(ImageProperty, value);
            }
        }

        public static readonly BindableProperty ButtonProperty = BindableProperty.Create(nameof(Button), typeof(ICommand), typeof(Register), default(ICommand), BindingMode.TwoWay);
        public ICommand Button
        {
            get
            {
                return (ICommand)GetValue(ButtonProperty);
            }

            set
            {
                SetValue(ButtonProperty, value);
            }
        }

        public static readonly BindableProperty ParameterProperty = BindableProperty.Create(nameof(Parameter), typeof(string), typeof(Register), default(string), Xamarin.Forms.BindingMode.TwoWay);
        public string Parameter
        {
            get
            {
                return (string)GetValue(ParameterProperty);
            }

            set
            {
                SetValue(ParameterProperty, value);
            }
        }

        public Register()
        {
            InitializeComponent();

            boxView.BackgroundColor = BoxView;
            text.Text = Text;
            image.Source = ImageSource.FromResource(textureLoader.GetProgrammingCardTexturePath(Image, PlayerModel.Instance.Color).Replace("/", "."));
            if (Image == Constants.programmingCardBack)
            {                
                image.Source = ImageSource.FromResource(textureLoader.GetProgrammingCardBackTexturePath().Replace("/", "."));
            }
            else
            {
                image.Source = ImageSource.FromResource(textureLoader.GetProgrammingCardTexturePath(Image, PlayerModel.Instance.Color).Replace("/", "."));
            }
            button.Command = Button;
            button.CommandParameter = Parameter;
            button.ClassId = Parameter;
        }

        protected override void OnPropertyChanged(string propertyName = null)
        {
            base.OnPropertyChanged(propertyName);
            if (propertyName == BoxViewProperty.PropertyName)
            {
                boxView.BackgroundColor = BoxView;
            }
            else if (propertyName == TextProperty.PropertyName)
            {
                text.Text = Text;
            }
            else if (propertyName == ImageProperty.PropertyName)
            {
                if (Image == Constants.programmingCardBack)
                {
                    image.Source = ImageSource.FromResource(textureLoader.GetProgrammingCardBackTexturePath().Replace("/", "."));
                }
                else
                {
                    image.Source = ImageSource.FromResource(textureLoader.GetProgrammingCardTexturePath(Image, PlayerModel.Instance.Color).Replace("/", "."));
                }
            }
            else if (propertyName == ButtonProperty.PropertyName)
            {
                button.Command = Button;
            }
            else if (propertyName == ParameterProperty.PropertyName)
            {
                button.CommandParameter = Parameter;
                button.ClassId = Parameter;
            }
        }
    }
}