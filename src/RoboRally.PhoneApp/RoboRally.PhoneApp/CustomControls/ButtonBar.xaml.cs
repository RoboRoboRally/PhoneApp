﻿using Xamarin.Forms.Xaml;

namespace RoboRally.PhoneApp.CustomControls
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class ButtonBar : Xamarin.Forms.Grid
    {
        public ButtonBar()
        {
            InitializeComponent();
        }
    }
}