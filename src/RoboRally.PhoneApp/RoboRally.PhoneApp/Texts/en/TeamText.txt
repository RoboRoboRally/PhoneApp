﻿Our team consist of 4 memebrs:

Václav Čamra wrote the server and handled communication between the the applications, model and the robot. He also printed custom parts on 3d printer for the robot.

Andrej Čižmárik assembled the robot and wrote the software, which controls it.

Adam Filandr created the mobile application and desktop application.

Petr Šťavík wrote the model - the implemetation of rules and representation of game in code.

If you would like to contact us, you can find us on LinkedIn.