﻿using RoboRoboRally.Model.Textures.CardTextures;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Converters
{
    /// <summary>
    /// Converter used to convert choice cards into texture ImageSource
    /// </summary>
    class ChoiceConverter : IValueConverter
    {
        CardTextureLoader textureLoader = new CardTextureLoader("RoboRally.PhoneApp/GameData/Textures/CardTextures");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return ImageSource.FromResource(textureLoader.GetChoiceCardTexturePath((string)parameter, value.ToString()).Replace("/", "."));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
