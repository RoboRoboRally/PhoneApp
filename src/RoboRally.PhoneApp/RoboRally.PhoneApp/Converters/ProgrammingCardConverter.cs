﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Model.Textures.CardTextures;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Converters
{
    /// <summary>
    /// Converter used to convert programming card CardId into texture ImageSource
    /// </summary>
    class ProgrammingCardConverter : IValueConverter
    {
        CardTextureLoader textureLoader = new CardTextureLoader("RoboRally.PhoneApp/GameData/Textures/CardTextures");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            if (value.ToString() == Constants.programmingCardBack)
            {
                return ImageSource.FromResource(textureLoader.GetProgrammingCardBackTexturePath().Replace("/", "."));
            }
            else
            {
                return ImageSource.FromResource(textureLoader.GetProgrammingCardTexturePath(value.ToString(), PlayerModel.Instance.Color).Replace("/", "."));
            }                        
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
