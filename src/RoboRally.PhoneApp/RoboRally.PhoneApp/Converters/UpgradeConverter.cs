﻿using RoboRoboRally.Model.Textures.CardTextures;
using System;
using System.Globalization;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Converters
{
    /// <summary>
    /// Converter used to convert upgrade CardId into a texture ImageSource
    /// </summary>
    class UpgradeConverter : IValueConverter
    {
        CardTextureLoader textureLoader = new CardTextureLoader("RoboRally.PhoneApp/GameData/Textures/CardTextures");

        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            return ImageSource.FromResource(textureLoader.GetUpgradeCardTexturePath(value.ToString()).Replace("/", "."));
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
