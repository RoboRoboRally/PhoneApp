﻿using System;
using System.Globalization;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Converters
{
    /// <summary>
    /// Converter used to convert int into time counter - values below zero are shown as "Timer off" string
    /// </summary>
    class TimerConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == null)
            {
                return null;
            }

            if ((int)value <= 0)
            {
                return "Timer off";
            }
            else
            {
                return value.ToString();
            }
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
