﻿using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.Views;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the ActivationPage
    /// On this page players can view their programmed cards and use upgrade cards    
    /// </summary>
    class ActivationViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> Registers { get; private set; } = PlayerModel.Instance.SelectedCards;
        public ObservableCollection<SlotModel> UpgradeCards { get; private set; } = new ObservableCollection<SlotModel>();
        public SlotModel SelectedCard { get; private set; }

        private bool sendEnabled = false;
        public bool SendEnabled
        {
            get { return sendEnabled; }
            set { SetValue(ref sendEnabled, value, nameof(SendEnabled)); }
        }

        private double sendOpacity = 0.5;
        public double SendOpacity
        {
            get { return sendOpacity; }
            set { SetValue(ref sendOpacity, value, nameof(SendOpacity)); }
        }

        private int selectedCardNum = 0;        
        public TaskCompletionSource<CardInfo> secondary_tcs;

        public ICommand BigCardClickedCommand { get; private set; }
        public ICommand RegisterClickedCommand { get; private set; }
        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand SendClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand QuitClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public ActivationViewModel()
        {
            PlayerModel.Instance.activationViewModel = this;

            // Display cards selected in programming phase 
            if (PlayerModel.Instance.SelectedCards.Count == Constants.selectedCardsNum)
            {
                for (int i = 0; i < Constants.selectedCardsNum; i++)
                {
                    Registers.Add(new SlotModel(Controller.registerColor, new CardModel(PlayerModel.Instance.SelectedCards[i].Card.CardId)));
                    Registers[i].Color = Color.LightGreen;
                }
            }
            
            foreach (var upgrade in PlayerModel.Instance.TemporaryUpgrades)
            {
                UpgradeCards.Add((new SlotModel(new CardModel(upgrade.CardId)) { availabe = false, Opacity = Constants.dimOpacity }));
            }

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectedCard.availabe = false;
            if (UpgradeCards.Count != 0)
            {
                NextClicked();
            }

            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            SendClickedCommand = new Command(async vm => await SendClicked());
            HelpClickedCommand = new Command(HelpClicked);
            QuitClickedCommand = new Command(QuitClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        public Task<CardInfo> SelectChoice(UpgradeCardInfo upgrade)
        {
            TaskCompletionSource<CardInfo> tcs = new TaskCompletionSource<CardInfo>();
            secondary_tcs = tcs;

            Device.BeginInvokeOnMainThread(() =>
            {
                Controller.CallPage(new ChoicePage(tcs, upgrade));
            });

            return WaitCardInfoTask(tcs);

        }

        private async Task<CardInfo> WaitCardInfoTask(TaskCompletionSource<CardInfo> tcs)
        {
            return await tcs.Task;
        }

        /// <summary>
        /// In case we use all available upgrades, disable the send button
        /// </summary>
        private void SendButtonToogle()
        {
            if (SelectedCard.availabe == true)
            {
                SendEnabled = true;
                SendOpacity = 1;
            }
            else
            {
                SendEnabled = false;
                SendOpacity = Constants.dimOpacity;
            }
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(UpgradeCards, SelectedCard, previousCard, newCard);
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(UpgradeCards, SelectedCard, ref selectedCardNum);
            SendButtonToogle();
        }

        public void NextClicked()
        {
            Controller.NextCard(UpgradeCards, SelectedCard, ref selectedCardNum);
            SendButtonToogle();
        }

        /// <summary>
        /// Use the async upgrade
        /// Send it right away if the upgrade has no choices
        /// If the upgrade has choices, give the player the option to select it 
        /// </summary>
        /// <returns></returns>
        private async Task SendClicked()
        {
            UpgradeCardInfo upgrade = PlayerModel.Instance.TemporaryUpgrades.Single(i => i.CardId == SelectedCard.Card.CardId);

            if (upgrade.Choices == null || upgrade.Choices.Length == 0)
            {
                await Task.Run(() => PlayerModel.Instance.game.PlayAsyncCard(upgrade));
            }
            else
            {                
                Task<CardInfo> choiceTask = SelectChoice(upgrade);
                CardInfo choice = null;
                await Task.Run(() => choice = choiceTask.Result);

                if (choice != null)
                {
                    await Task.Run(() => PlayerModel.Instance.game.PlayAsyncCard(upgrade, choice));                                                        
                }
                else
                {
                    return;
                }
            }
        }

        /// <summary>
        /// If we receive confirmation from the server, that the cards has been used,
        /// remove the texture and make it not selectable with arrow buttons 
        /// </summary>
        /// <param name="cardId">which card to disable</param>
        public void DisableCard(string cardId)
        {
            try
            {
                if (SelectedCard.Card.CardId == cardId)
                {
                    SelectedCard.Card = new CardModel("");
                    SelectedCard.availabe = false;
                }

                SlotModel card = UpgradeCards.Single(i => i.Card.CardId == cardId);
                card.availabe = false;
                card.Card = new CardModel("");
                card.Color = Controller.backgroundColor;

                // Select next card
                NextClicked();
            }
            catch (Exception) { }
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.ActivationHelp, "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();            
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }
    }
}
