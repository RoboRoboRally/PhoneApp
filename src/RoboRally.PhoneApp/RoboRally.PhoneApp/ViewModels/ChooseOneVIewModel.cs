﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the ChoosePage
    /// On this page players can choose from multiple offered programming cards    
    /// </summary>
    class ChooseOneViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> OfferedCards { get; private set; } = PlayerModel.Instance.ReceivedCards;
        public SlotModel SelectedCard { get; private set; }

        private bool sendEnabled;
        public bool SendEnabled
        {
            get { return sendEnabled; }
            set { SetValue(ref sendEnabled, value, nameof(SendEnabled)); }
        }

        private double sendOpacity;
        public double SendOpacity
        {
            get { return sendOpacity; }
            set { SetValue(ref sendOpacity, value, nameof(SendOpacity)); }
        }

        private int selectedCardNum = 0;
        private TaskCompletionSource<CardInfo[]> tcs;

        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand SendClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand QuitClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public ChooseOneViewModel(TaskCompletionSource<CardInfo[]> tcs)
        {
            this.tcs = tcs;
            PlayerModel.Instance.action = PlayerModel.ActionType.ChooseOne;                   

            SendEnabled = true;
            SendOpacity = 1;

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            SendClickedCommand = new Command(SendClicked);
            HelpClickedCommand = new Command(HelpClicked);
            QuitClickedCommand = new Command(QuitClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(OfferedCards, SelectedCard, previousCard, newCard);
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(OfferedCards, SelectedCard, ref selectedCardNum);
        }

        private void NextClicked()
        {
            Controller.NextCard(OfferedCards, SelectedCard, ref selectedCardNum);
        }

        /// <summary>
        /// We return an array of 1, because we reuse the server call for ChooseProgrammingCards, which needs an array as a return
        /// </summary>
        private void SendClicked()
        {
            sendEnabled = false;
            SendOpacity = Constants.dimOpacity;

            tcs.TrySetResult(new CardInfo[1] { new CardInfo() { CardId = SelectedCard.Card.CardId } });
            PlayerModel.Instance.Clean();
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.ChooseOneHelp, "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }
    }
}
