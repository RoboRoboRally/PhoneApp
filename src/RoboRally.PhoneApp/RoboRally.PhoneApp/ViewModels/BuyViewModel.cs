﻿using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.Views;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the BuyPage
    /// On this page players can buy new upgrades
    /// </summary>
    class BuyViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> UpgradeCards { get; private set; } = new ObservableCollection<SlotModel>();
        public SlotModel SelectedCard { get; private set; }
        private PurchaseOffer offer = null;

        private int selectedCardNum = 0;
        private TaskCompletionSource<PurchaseResponse> tcs;

        public ICommand YesClickedCommand { get; private set; }
        public ICommand NoClickedCommand { get; private set; }
        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand QuitClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public BuyViewModel(TaskCompletionSource<PurchaseResponse> tcs, PurchaseOffer offer)
        {
            this.tcs = tcs;
            this.offer = offer;
            PlayerModel.Instance.action = PlayerModel.ActionType.Buying;            

            foreach (var upgrade in offer.OfferedCards)
            {
                UpgradeCards.Add(new SlotModel(new Models.CardModel(upgrade.CardId)));
            }

            SelectedCard = new SlotModel(Controller.backgroundColor, new Models.CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            YesClickedCommand = new Command(YesClicked);
            NoClickedCommand = new Command(NoClicked);
            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            HelpClickedCommand = new Command(HelpClicked);
            QuitClickedCommand = new Command(QuitClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        private async Task<CardInfo> WaitCardInfoTask(TaskCompletionSource<CardInfo> tcs)
        {
            return await tcs.Task;
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(UpgradeCards, SelectedCard, previousCard, newCard);
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        private void NextClicked()
        {
            Controller.NextCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        /// <summary>
        /// Check if player has enough batteries and that he did not reach the limit of upgrade cards
        /// If he reached the limit, show him the DiscardPage, where he can decide to discard a card
        /// </summary>
        private async void YesClicked()
        {
            UpgradeCardInfo upgrade = offer.OfferedCards.Single(i => i.CardId == SelectedCard.Card.CardId);

            if (PlayerModel.Instance.Batteries < upgrade.Cost)
            {
                Controller.Help(AppResources.CostMessage, AppResources.NotEnoughBateries, "", AppResources.Ok);
                return;
            }

            ObservableCollection<UpgradeCardInfo> playerUpgrades;
            if (upgrade.CardType == UpgradeCardType.Permanent)
            {
                playerUpgrades = PlayerModel.Instance.PermanentUpgrades;
            }
            else
            {
                playerUpgrades = PlayerModel.Instance.TemporaryUpgrades;
            }

            if (PlayerModel.Instance.OwnedCardLimit[upgrade.CardType] <= playerUpgrades.Count)
            {
                Task<CardInfo> discardedTask = DiscardUpgrade(playerUpgrades);
                CardInfo discardedCard = null;
                await Task.Run(() => discardedCard = discardedTask.Result);

                try
                {
                    UpgradeCardInfo discardedUpgradeCardInfo = playerUpgrades.Single(i => i.CardId == discardedCard.CardId);
                    tcs.TrySetResult(new PurchaseResponse { SelectedCard = upgrade, DiscardedCard = discardedUpgradeCardInfo });
                }
                catch (Exception)
                {
                    return;
                }
            }
            else
            {
                tcs.TrySetResult(new PurchaseResponse { SelectedCard = upgrade });
            }

            PlayerModel.Instance.Clean();
        }

        private Task<CardInfo> DiscardUpgrade(ObservableCollection<UpgradeCardInfo> upgrades)
        {
            TaskCompletionSource<CardInfo> tcs = new TaskCompletionSource<CardInfo>();
            PlayerModel.Instance.completionDiscard = tcs;
            Device.BeginInvokeOnMainThread(() =>
            {
                Controller.CallPage(new DiscardPage(tcs, upgrades));
            });
            return WaitCardInfoTask(PlayerModel.Instance.completionDiscard);
        }

        private void NoClicked()
        {
            tcs.TrySetResult(new PurchaseResponse());
            PlayerModel.Instance.Clean();
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.BuyHelp, "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }
    }
}