﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the ChoicePage
    /// On this page players can choose from multiple offered upgrade cards
    /// </summary>
    class ChoiceViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> UpgradeCards { get; private set; } = new ObservableCollection<SlotModel>();
        public SlotModel SelectedCard { get; private set; }

        private int selectedCardNum = 0;
        private TaskCompletionSource<CardInfo> tcs;

        public ICommand YesClickedCommand { get; private set; }
        public ICommand NoClickedCommand { get; private set; }
        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand QuitClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public ChoiceViewModel(TaskCompletionSource<CardInfo> tcs, UpgradeCardInfo card)
        {
            this.tcs = tcs;
            PlayerModel.Instance.action = PlayerModel.ActionType.Choice;            

            foreach (var choice in card.Choices)
            {
                UpgradeCards.Add(new SlotModel(new CardModel(choice.CardId)));
            }

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            YesClickedCommand = new Command(YesClicked);
            NoClickedCommand = new Command(NoClicked);
            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            HelpClickedCommand = new Command(HelpClicked);
            QuitClickedCommand = new Command(QuitClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(UpgradeCards, SelectedCard, previousCard, newCard);
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        private void NextClicked()
        {
            Controller.NextCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        private void YesClicked()
        {
            tcs.TrySetResult(new CardInfo { CardId = SelectedCard.Card.CardId });
            PlayerModel.Instance.Clean();
        }

        private void NoClicked()
        {
            tcs.TrySetResult(null);
            PlayerModel.Instance.Clean();
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.ChoiceHelp, "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }
    }
}