﻿using Newtonsoft.Json;
using RoboRally.PhoneApp.Communication;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.Views;
using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// ViewModel for the InventoryPage
    /// Handles buttons for Play, Rules, About and scan of the QR code
    /// </summary>
    class IntroViewModel
    {
        public ICommand AboutClickedCommand { get; private set; }
        public ICommand RulesClickedCommand { get; private set; }
        public ICommand PlayClickedCommand { get; private set; }

        public IntroViewModel()
        {
            AboutClickedCommand = new Command(AboutClicked);
            RulesClickedCommand = new Command(RulesClicked);
            PlayClickedCommand = new Command(PlayClicked);
        }

        async void AboutClicked()
        {
            await PageService.Instance.PushModalAsync(new AboutPage());
        }

        async void RulesClicked()
        {
            await PageService.Instance.PushModalAsync(new RulesPage());
        }

        void PlayClicked()
        {
            QRScan();
        }

        private async void QRScan()
        {
            PlayerModel.Instance.NewGame();

            // Extract data from QR code
            // When using back button during scanning an exception is thrown
            ConnectionData data = null;
            try
            {
                var scanner = new ZXing.Mobile.MobileBarcodeScanner();
                scanner.BottomText = AppResources.QR;
                string resultText = (await scanner.Scan()).Text;
                data = JsonConvert.DeserializeObject<ConnectionData>(resultText);
            }
            catch (Exception)
            {
                return;
            }

            // Try to connect, show error message if not successful
            try
            {
                await PageService.Instance.PushModalAsync(new WaitingPage(AppResources.Connecting));
                await Task.Run(() => Server.connect(new UriBuilder("ws", data.Ip, data.Port).Uri, data.LobbyName, data.RobotName));
                await PageService.Instance.PopModalAsync();
            }
            catch (Exception)
            {
                await PageService.Instance.PopModalAsync();
                await PageService.Instance.DisplayAlert(AppResources.ConnectionErrorTitle, AppResources.ConnectionErrorMsg, "", AppResources.Ok);
                return;
            }
            await PageService.Instance.PushModalAsync(new WaitingPage(AppResources.Waiting));
        }
    }
}
