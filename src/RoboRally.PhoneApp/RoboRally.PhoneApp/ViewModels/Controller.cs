﻿using RoboRally.PhoneApp.Communication;
using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.Views;
using System.Collections.ObjectModel;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// In this static class we implement basic button functionality, which is used in most ViewModels
    /// ViewModels mostly use the same set of buttons, this way the code is easier to maintain. They differ mostly in their send button.
    /// </summary>
    static class Controller
    {
        public static Color registerColor = Color.Azure;
        public static Color backgroundColor = Color.RoyalBlue;
        public static Color markedColor = Color.Yellow;
        public static int depth = 0;
        public static object depthLock = new object();

        /// <summary>
        /// Assigns a card from a collection into a single instance
        /// Used for assignment of cards from the top line into the central big card
        /// </summary>
        /// <param name="cards">Collection of cards - upgrades or programming cards</param>
        /// <param name="selectedCard">Instance of selected card</param>
        /// <param name="previousCard">Index of previous selected card in collection</param>
        /// <param name="newCard">Index of new selected card in collection</param>
        public static void SelectCard(ObservableCollection<SlotModel> cards, SlotModel selectedCard, int previousCard, int newCard)
        {
            if (cards.Count > 0)
            {
                cards[previousCard].Color = backgroundColor;
                cards[newCard].Color = markedColor;
                selectedCard.Card = cards[newCard].Card;
                selectedCard.availabe = true;
            }
        }

        /// <summary>
        /// Use to move through collection of cards backwards
        /// Has a check in case no card is available, in order to not cycle forever
        /// </summary>
        /// <param name="cards">Collection of cards - upgrades or programming cards</param>
        /// <param name="selectedCard">Instance of selected card</param>
        /// <param name="selectedCardNum">Index of selected card in collection</param>
        public static void PreviousCard(ObservableCollection<SlotModel> cards, SlotModel selectedCard, ref int selectedCardNum)
        {
            int nextCard = selectedCardNum - 1;
            if (nextCard < 0)
                nextCard = cards.Count - 1;

            int c = 0;
            while (cards[nextCard].availabe != true)
            {
                nextCard--;
                if (nextCard < 0)
                    nextCard = cards.Count - 1;

                // to stop cycling forever, if no card available
                if (c > cards.Count)
                {
                    return;
                }
                c++;
            }

            SelectCard(cards, selectedCard, selectedCardNum, nextCard);
            selectedCardNum = nextCard;
            selectedCard.Color = backgroundColor;
        }

        /// <summary>
        /// Use to move through collection of cards forward
        /// Has a check in case no card is available, in order to not cycle forever
        /// </summary>
        /// <param name="cards">Collection of cards - upgrades or programming cards</param>
        /// <param name="selectedCard">Instance of selected card</param>
        /// <param name="selectedCardNum">Index of selected card in collection</param>
        public static void NextCard(ObservableCollection<SlotModel> cards, SlotModel selectedCard, ref int selectedCardNum)
        {
            int nextCard = selectedCardNum + 1;
            if (nextCard >= cards.Count)
                nextCard = 0;

            int c = 0;
            while (cards[nextCard].availabe != true)
            {
                nextCard++;
                if (nextCard >= cards.Count)
                    nextCard = 0;

                // to stop cycling forever, if no card available
                if (c > cards.Count)
                {
                    return;
                }
                c++;
            }

            SelectCard(cards, selectedCard, selectedCardNum, nextCard);
            selectedCardNum = nextCard;
            selectedCard.Color = backgroundColor;
        }

        /// <summary>
        /// Used mainly to show a help message, can be used to show other pop-up message
        /// </summary>
        /// <param name="title"></param>
        /// <param name="message"></param>
        /// <param name="ok"></param>
        /// <param name="cancel"></param>
        public async static void Help(string title, string message, string ok, string cancel)
        {
            await PageService.Instance.DisplayAlert(title, message, ok, cancel);
        }

        public static void Back()
        {
            PopPage();
            Controller.DecreaseDepth();
        }

        /// <summary>
        /// Use to quit the game
        /// In case we are in activation phase, we need to remove the activation flag - otherwise we would not remove all pages
        /// </summary>
        public async static void Quit()
        {
            var result = await PageService.Instance.DisplayAlert(AppResources.QuitMessage, AppResources.QuitText, AppResources.Yes, AppResources.No);

            if (result == true)
            {
                try
                {
                    PlayerModel.Instance.activation = false;
                    Server.disconnect();
                    PlayerModel.Instance.Clean(1, true);
                }
                catch { }
            }
        }

        /// <summary>
        /// Used to show the inventory
        /// </summary>
        public static void Inventory()
        {
            CallPage(new InventoryPage());
        }

        /// <summary>
        /// Used to push new page
        /// </summary>
        /// <param name="page">New page</param>
        public async static void CallPage(Page page)
        {
            await PageService.Instance.PushModalAsync(page);
            IncreaseDepth();
        }

        public async static void PopPage()
        {
            await PageService.Instance.PopModalAsync();
        }

        /// <summary>
        /// The whole process or page removal has to be locked - this prevents two threads removing more pages than intended.
        /// Multiple page removals at the same time could be triggered by communication with server
        /// </summary>
        /// <param name="c"></param>
        public static void PopPages(int c = 0)
        {
            // We want to have the activation screen on top
            if (PlayerModel.Instance.activation == true)
            {
                c--;
            }

            lock (depthLock)
            {
                for (int i = 0; i < depth + c; i++)
                {
                    PopPage();
                }
                depth = 0;

                // count the one page again
                if (PlayerModel.Instance.activation == true)
                {
                    depth++;
                }
            }
        }

        /// <summary>
        /// Locked doue to the possibility of multiple threads manipulating depth, which could interfere with PopPages
        /// </summary>
        public static void IncreaseDepth()
        {
            lock (Controller.depthLock)
            {
                depth++;
            }
        }

        /// <summary>
        /// Locked doue to the possibility of multiple threads manipulating depth, which could interfere with PopPages
        /// </summary>
        public static void DecreaseDepth()
        {
            lock (Controller.depthLock)
            {
                depth--;
            }
        }
    }
}
