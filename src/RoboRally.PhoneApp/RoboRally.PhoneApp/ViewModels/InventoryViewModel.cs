﻿using RoboRally.PhoneApp.Models;
using System.Collections.ObjectModel;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the InventoryPage
    /// On this page players can view their upgrades, batteries and checkpoints
    /// </summary>
    class InventoryViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> UpgradeCards { get; private set; } = new ObservableCollection<SlotModel>();
        public SlotModel SelectedCard { get; private set; }

        private int selectedCardNum = 0;

        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand BackClickedCommand { get; private set; }

        public InventoryViewModel()
        {           
            foreach (var upgradeModel in PlayerModel.Instance.TemporaryUpgrades)
            {
                UpgradeCards.Add(new SlotModel(new CardModel(upgradeModel.CardId)));
            }

            foreach (var upgradeModel in PlayerModel.Instance.PermanentUpgrades)
            {
                UpgradeCards.Add(new SlotModel(new CardModel(upgradeModel.CardId)));
            }

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            HelpClickedCommand = new Command(HelpClicked);
            BackClickedCommand = new Command(BackClicked);
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(UpgradeCards, SelectedCard, previousCard, newCard);
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        private void NextClicked()
        {
            Controller.NextCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.InventoryHelp, "", AppResources.Ok);
        }

        private void BackClicked()
        {
            Controller.Back();
        }
    }
}
