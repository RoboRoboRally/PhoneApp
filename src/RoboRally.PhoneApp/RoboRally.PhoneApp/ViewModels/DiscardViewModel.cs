﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the DiscardPage
    /// On this page players discard a card    
    /// </summary>
    class DiscardViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> UpgradeCards { get; private set; } = new ObservableCollection<SlotModel>();
        public SlotModel SelectedCard { get; private set; }

        private int selectedCardNum = 0;
        private TaskCompletionSource<CardInfo> tcs;

        public ICommand YesClickedCommand { get; private set; }
        public ICommand NoClickedCommand { get; private set; }
        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand BackClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public DiscardViewModel(TaskCompletionSource<CardInfo> tcs, ObservableCollection<UpgradeCardInfo> upgradeModels)
        {
            this.tcs = tcs;
            PlayerModel.Instance.action = PlayerModel.ActionType.Discard;            

            foreach (var upgradeModel in upgradeModels)
            {
                UpgradeCards.Add(new SlotModel(new CardModel(upgradeModel.CardId)));
            }

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            YesClickedCommand = new Command(YesClicked);
            NoClickedCommand = new Command(NoClicked);
            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            HelpClickedCommand = new Command(HelpClicked);
            BackClickedCommand = new Command(BackClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(UpgradeCards, SelectedCard, previousCard, newCard);
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        private void NextClicked()
        {
            Controller.NextCard(UpgradeCards, SelectedCard, ref selectedCardNum);
        }

        /// <summary>
        /// Send the selected card
        /// </summary>
        private void YesClicked()
        {
            tcs.TrySetResult(new CardInfo { CardId = SelectedCard.Card.CardId });
            Controller.Back();
            PlayerModel.Instance.action = PlayerModel.ActionType.Buying;
        }

        /// <summary>
        /// Just return to previous screen
        /// </summary>
        private void NoClicked()
        {
            tcs.TrySetResult(null);
            Controller.Back();
            PlayerModel.Instance.action = PlayerModel.ActionType.Buying;
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.DiscardHelp, "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }

        private void BackClicked()
        {
            Controller.Back();
        }
    }
}