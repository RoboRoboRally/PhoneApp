﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the ChoicelessPage
    /// On this page players can choose to use, or not to use, an upgrade    
    /// </summary>
    class ChoicelessViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> UpgradeCards { get; private set; } = new ObservableCollection<SlotModel>();
        public SlotModel SelectedCard { get; private set; }

        private int selectedCardNum = 0;
        private TaskCompletionSource<bool> tcs;

        public ICommand YesClickedCommand { get; private set; }
        public ICommand NoClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand QuitClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public ChoicelessViewModel(TaskCompletionSource<bool> tcs, CardInfo card)
        {
            this.tcs = tcs;
            PlayerModel.Instance.action = PlayerModel.ActionType.Choiceless;            

            UpgradeCards.Add(new SlotModel(new CardModel(card.CardId)));

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            YesClickedCommand = new Command(YesClicked);
            NoClickedCommand = new Command(NoClicked);
            HelpClickedCommand = new Command(HelpClicked);
            QuitClickedCommand = new Command(QuitClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(UpgradeCards, SelectedCard, previousCard, newCard);
        }

        private void YesClicked()
        {
            tcs.TrySetResult(true);
            PlayerModel.Instance.Clean();
        }

        private void NoClicked()
        {
            tcs.TrySetResult(false);
            PlayerModel.Instance.Clean();
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, AppResources.ChoicelessHelp, "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }
    }
}