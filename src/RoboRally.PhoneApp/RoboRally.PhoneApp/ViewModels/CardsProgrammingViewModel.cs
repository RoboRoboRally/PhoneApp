﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Server.Interfaces;
using System.Collections.ObjectModel;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.ViewModels
{
    /// <summary>
    /// Viewmodel for the CardProgrammingPage
    /// On this page players can select programming cards    
    /// </summary>
    class CardsProgrammingViewModel : BaseModel
    {
        public ObservableCollection<SlotModel> Registers { get; private set; } = PlayerModel.Instance.SelectedCards;
        public ObservableCollection<SlotModel> OfferedCards { get; private set; } = PlayerModel.Instance.ReceivedCards;
        public SlotModel SelectedCard { get; private set; }

        private bool sendEnabled;
        public bool SendEnabled
        {
            get { return sendEnabled; }
            set { SetValue(ref sendEnabled, value, nameof(SendEnabled)); }
        }

        private double sendOpacity;
        public double SendOpacity
        {
            get { return sendOpacity; }
            set { SetValue(ref sendOpacity, value, nameof(SendOpacity)); }
        }

        private bool movigRegister = false;
        private int selectedRegisterNum = 0;
        private bool movingCard = false;
        private int selectedCardNum = 0;
        private TaskCompletionSource<CardInfo[]> tcs;

        public ICommand BigCardClickedCommand { get; private set; }
        public ICommand RegisterClickedCommand { get; private set; }
        public ICommand PreviousClickedCommand { get; private set; }
        public ICommand NextClickedCommand { get; private set; }
        public ICommand SendClickedCommand { get; private set; }
        public ICommand HelpClickedCommand { get; private set; }
        public ICommand QuitClickedCommand { get; private set; }
        public ICommand InventoryClickedCommand { get; private set; }

        public CardsProgrammingViewModel(TaskCompletionSource<CardInfo[]> tcs)
        {
            this.tcs = tcs;
            PlayerModel.Instance.action = PlayerModel.ActionType.Programming;            

            SendEnabled = false;
            SendOpacity = Constants.dimOpacity;

            for (int i = 0; i < Constants.selectedCardsNum; i++)
            {
                Registers.Add(new SlotModel(Controller.registerColor, new CardModel("")));
            }

            SelectedCard = new SlotModel(Controller.backgroundColor, new CardModel(""));
            SelectCard(selectedCardNum, selectedCardNum);

            BigCardClickedCommand = new Command(BigCardClicked);
            RegisterClickedCommand = new Command<string>(RegisterClicked);
            PreviousClickedCommand = new Command(PreviousClicked);
            NextClickedCommand = new Command(NextClicked);
            SendClickedCommand = new Command(SendClicked);
            HelpClickedCommand = new Command(HelpClicked);
            QuitClickedCommand = new Command(QuitClicked);
            InventoryClickedCommand = new Command(InventoryClicked);
        }

        private void SelectCard(int previousCard, int newCard)
        {
            Controller.SelectCard(OfferedCards, SelectedCard, previousCard, newCard);
        }

        /// <summary>
        /// When the user clicks the reigster, we have to decide
        /// if he was moving selected card into the register
        /// or if he was moving cards between registers.     
        /// Also in a case all registers are filled, enable the send button
        /// </summary>
        /// <param name="num"></param>
        private void RegisterClicked(string num)
        {
            int registerNum = int.Parse(num);

            if (movingCard)
            {
                // Free previous card
                if (Registers[registerNum].used)
                {
                    OfferedCards[Registers[registerNum].cardNum].availabe = true;
                    OfferedCards[Registers[registerNum].cardNum].Opacity = 1;
                }
                else { Registers[registerNum].used = true; }

                // Put new card in                
                Registers[registerNum].Card = SelectedCard.Card;
                Registers[registerNum].cardNum = selectedCardNum;
                OfferedCards[selectedCardNum].Opacity = Constants.dimOpacity;
                OfferedCards[selectedCardNum].availabe = false;

                movingCard = false;
                SelectedCard.Card = null;
                SelectedCard.Color = Controller.backgroundColor;
                NextClicked();
            }
            else
            {
                if (movigRegister)
                {
                    //Swap
                    Registers[selectedRegisterNum].Color = Controller.registerColor;
                    CardModel cardHolder = Registers[registerNum].Card;
                    int numHolder = Registers[registerNum].cardNum;
                    bool usedHolder = Registers[registerNum].used;
                    Registers[registerNum].Card = Registers[selectedRegisterNum].Card;
                    Registers[registerNum].cardNum = Registers[selectedRegisterNum].cardNum;
                    Registers[registerNum].used = Registers[selectedRegisterNum].used;
                    Registers[selectedRegisterNum].Card = cardHolder;
                    Registers[selectedRegisterNum].cardNum = numHolder;
                    Registers[selectedRegisterNum].used = usedHolder;
                    movigRegister = false;
                }
                else
                {
                    movigRegister = true;
                    selectedRegisterNum = registerNum;
                    Registers[registerNum].Color = Controller.markedColor;
                }
            }

            // Enable send if enough cards selected
            if (SendEnabled == false)
            {
                int counter = 0;
                foreach (SlotModel item in Registers)
                {
                    if (item.used == true)
                    {
                        counter++;
                    }
                }
                if (counter == Constants.selectedCardsNum)
                {
                    SendEnabled = true;
                    SendOpacity = 1;
                }
            }
        }

        /// <summary>
        /// Selection and deselection of central card
        /// </summary>
        private void BigCardClicked()
        {
            if (SelectedCard.availabe && !(SelectedCard.Card is null))
            {
                if (movigRegister)
                {
                    // Reselect
                    movigRegister = false;
                    Registers[selectedRegisterNum].Color = Controller.registerColor;
                    movingCard = true;
                    SelectedCard.Color = Controller.markedColor;
                    return;
                }

                if (movingCard)
                {
                    //Deselect
                    movingCard = false;
                    SelectedCard.Color = Controller.backgroundColor;
                }
                else
                {
                    //Select
                    movingCard = true;
                    SelectedCard.Color = Controller.markedColor;
                }
            }
        }

        private void PreviousClicked()
        {
            Controller.PreviousCard(OfferedCards, SelectedCard, ref selectedCardNum);
            movingCard = false;
        }

        private void NextClicked()
        {
            Controller.NextCard(OfferedCards, SelectedCard, ref selectedCardNum);
            movingCard = false;
        }

        /// <summary>
        /// Send cards stored in the registers
        /// </summary>
        private void SendClicked()
        {
            sendEnabled = false;
            SendOpacity = Constants.dimOpacity;

            CardInfo[] info = new CardInfo[Constants.selectedCardsNum];            
            for (int i = 0; i < Constants.selectedCardsNum; i++)
            {
                info[i] = new CardInfo() { CardId = Registers[i].Card.CardId };
            }
            tcs.TrySetResult(info);
            PlayerModel.Instance.Clean();
        }

        private void HelpClicked()
        {
            Controller.Help(AppResources.HelpMessage, TextLoader.LoadText(AppResources.HelpText), "", AppResources.Ok);
        }

        private void QuitClicked()
        {
            Controller.Quit();
        }

        private void InventoryClicked()
        {
            Controller.Inventory();
        }
    }
}
