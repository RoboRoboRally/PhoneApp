﻿using System.IO;
using System.Reflection;

namespace RoboRally.PhoneApp.Models
{
    /// <summary>
    /// Class used to load larger texts of the application
    /// Texts loaded using this class include programming cards page help message, about screen and rules screen
    /// </summary>
    static class TextLoader
    {
        public static string LoadText(string file)
        {
            var assembly = IntrospectionExtensions.GetTypeInfo(typeof(TextLoader)).Assembly;
            Stream stream = assembly.GetManifestResourceStream("RoboRally.PhoneApp." + file);
            string text = "";
            using (var reader = new StreamReader(stream))
            {
                text = reader.ReadToEnd();
            }
            return text;
        }
    }
}
