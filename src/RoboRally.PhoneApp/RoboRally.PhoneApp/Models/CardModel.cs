﻿namespace RoboRally.PhoneApp.Models
{
    /// <summary>
    /// Class used to hold CardId of a card on the screen
    /// </summary>
    class CardModel
    {
        public string CardId { get; set; }

        public CardModel(string CardId)
        {
            this.CardId = CardId;
        }
    }
}
