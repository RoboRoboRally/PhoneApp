﻿using RoboRally.PhoneApp.Interfaces;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Models
{    
    /// <summary>
    /// We would prefer this class to be static, but the interface implemented prohibits this
    /// As a result we implemented this class as a singleton for easier use in the rest of the application.
    /// </summary>
    class PageService : IPageService
    {
        private static PageService instance = new PageService();
        public static PageService Instance { get { return instance; } }

        public async Task<bool> DisplayAlert(string title, string message, string ok, string cancel)
        {
            return await Application.Current.MainPage.DisplayAlert(title, message, ok, cancel);
        }

        public async Task PushModalAsync(Page page)
        {
            await Application.Current.MainPage.Navigation.PushModalAsync(page);
        }

        public async Task PopModalAsync()
        {
            await Application.Current.MainPage.Navigation.PopModalAsync();
        }
    }
}
