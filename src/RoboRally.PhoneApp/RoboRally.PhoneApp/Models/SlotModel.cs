﻿using Xamarin.Forms;

namespace RoboRally.PhoneApp.Models
{
    /// <summary>
    /// This class represents the card on the screen.
    /// It holds values needed to properly display the card, such as background color, opacity a the availability of the slot
    /// </summary>
    class SlotModel : BaseModel
    {
        /// <summary>
        /// Color used for the card background
        /// </summary>
        private Color color;
        public Color Color
        {
            get { return color; }
            set { SetValue(ref color, value, nameof(Color)); }
        }

        private CardModel card;
        public CardModel Card
        {
            get { return card; }
            set { SetValue(ref card, value, nameof(Card)); }
        }

        private double opacity = 1;
        public double Opacity
        {
            get { return opacity; }
            set { SetValue(ref opacity, value, nameof(Opacity)); }
        }

        public bool availabe = true;
        public bool used = false;

        public int cardNum = -1;

        public SlotModel(Color color, CardModel card)
        {
            this.color = color;
            this.Card = card;
        }

        public SlotModel(CardModel card)
        {
            this.color = Color.RoyalBlue;
            this.Card = card;
        }
    }
}
