﻿namespace RoboRally.PhoneApp.Models
{
    /// <summary>
    /// Few constats which we used in the appliaction
    /// </summary>
    static class Constants
    {
        public static int initialBatteries = 5;
        public static double dimOpacity = 0.5;
        public static string programmingCardBack = "ProgrammingCardBack";
        public static int selectedCardsNum = 5;
    }
}
