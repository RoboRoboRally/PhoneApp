﻿using Plugin.Vibrate;
using RoboRally.PhoneApp.ViewModels;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading;
using System.Threading.Tasks;

namespace RoboRally.PhoneApp.Models
{
    /// <summary>
    /// We chose PlayerModel to be a singleton in order to simplify acces to the properties
    /// This model is accesed from various places, it is a central model to the app
    /// </summary>
    class PlayerModel : BaseModel
    {
        private static PlayerModel instance = new PlayerModel();
        public static PlayerModel Instance { get { return instance; } }

        public IGame game = null;

        public enum ActionType { Buying, Programming, Choiceless, Choice, Discard, ChooseOne, None };
        public ActionType action;
        public bool activation = false;

        public RobotInfo robot = null;

        private int batteries = Constants.initialBatteries;
        public int Batteries
        {
            get { return batteries; }
            set { SetValue(ref batteries, value, nameof(Batteries)); }
        }

        private int checkpoints = 0;
        public int Checkpoints
        {
            get { return checkpoints; }
            set { SetValue(ref checkpoints, value, nameof(Checkpoints)); }
        }

        private int timer;
        public int Timer
        {
            get { return timer; }
            set { SetValue(ref timer, value, nameof(Timer)); }
        }

        private bool done = true;          

        public string Color = "";
        public ObservableCollection<SlotModel> ReceivedCards { get; private set; } = new ObservableCollection<SlotModel>();
        public ObservableCollection<SlotModel> SelectedCards { get; private set; } = new ObservableCollection<SlotModel>();

        public ObservableCollection<UpgradeCardInfo> TemporaryUpgrades { get; private set; } = new ObservableCollection<UpgradeCardInfo>();
        public ObservableCollection<UpgradeCardInfo> PermanentUpgrades { get; private set; } = new ObservableCollection<UpgradeCardInfo>();
        public Dictionary<UpgradeCardType, int> OwnedCardLimit = null;

        public TaskCompletionSource<CardInfo[]> completionProgramming = null;
        public TaskCompletionSource<PurchaseResponse> completionPurchaseResponse = null;
        public TaskCompletionSource<bool> completionChoiceless = null;
        public TaskCompletionSource<CardInfo> completionChoice = null;
        public TaskCompletionSource<CardInfo> completionDiscard = null;

        public ActivationViewModel activationViewModel = null;

        public void Erase()
        {
            ReceivedCards.Clear();
            SelectedCards.Clear();
            TemporaryUpgrades.Clear();
            PermanentUpgrades.Clear();
            Batteries = Constants.initialBatteries;
            Checkpoints = 0;
            OwnedCardLimit = null;
            done = true;
            Timer = 0;
        }

        public void NewGame()
        {
            ReceivedCards.Clear();
            SelectedCards.Clear();
            TemporaryUpgrades.Clear();
            PermanentUpgrades.Clear();
            OwnedCardLimit = null;
            Batteries = Constants.initialBatteries;
            Checkpoints = 0;
            activation = false;
            done = true;
            Timer = 0;
            game = null;
        }

        public void GameEnded()
        {
            PlayerModel.Instance.Timer = 0;
            PlayerModel.Instance.game = null;
            PlayerModel.Instance.action = PlayerModel.ActionType.None;
            activation = false;
        }

        public void Clean(int c = 0, bool finish = false)
        {
            Controller.PopPages(c);
            done = true;
            Timer = 0;

            if (finish)
            {
                try
                {
                    Finish();
                }
                catch (Exception) { }
            }
        }

        public void Init(TimeSpan timeout)
        {
            // To deactivate previous timer
            Timer = 0;
            Thread.Sleep(1000);

            ReceivedCards.Clear();
            if (activation == false)
            {
                SelectedCards.Clear();
            }

            done = false;
            Timer = (int)timeout.TotalSeconds - 2;
            Task.Run(() => RunTimer());

            //We need to wait in case other pages are being pushed or popped            
            Thread.Sleep(1000);

            try
            {
                var v = CrossVibrate.Current;
                v.Vibration(TimeSpan.FromSeconds(0.35));
            }
            catch (Exception)
            {

            }
        }

        static PlayerModel() { }
        private PlayerModel()
        {
        }

        private void Finish()
        {
            switch (action)
            {
                case ActionType.Buying:
                    finishBuying();
                    break;
                case ActionType.Programming:
                    finishProgramming();
                    break;
                case ActionType.Choiceless:
                    finishChoiceless();
                    break;
                case ActionType.Choice:
                    finishChoice();
                    break;
                case ActionType.Discard:
                    finishDiscard();
                    break;
                case ActionType.ChooseOne:
                    finishChooseOne();
                    break;
                default:
                    break;
            }
        }

        private void RunTimer()
        {
            bool full = false;
            while (Timer > 0 && done == false)
            {
                // Half second tick
                Thread.Sleep(500);
                if (full)
                {
                    Timer = Timer - 1;
                    full = false;
                }
                else
                {
                    full = true;
                }
            }

            // Timeout went off without finishing action
            if (done == false)
            {
                Clean(0, true);
            }
        }

        /// <summary>
        /// If all registers are used, send those cards to the server
        /// Otherwise send empty result
        /// </summary>
        private void finishProgramming()
        {
            // If the player chose all 5 cards, send them
            bool allCardsChosen = true;
            CardInfo[] info = new CardInfo[Constants.selectedCardsNum];
            for (int i = 0; i < Constants.selectedCardsNum; i++)
            {
                // If the card is not empty
                if (SelectedCards[i].Card.CardId != "")
                {
                    info[i] = new CardInfo() { CardId = SelectedCards[i].Card.CardId };
                }
                else
                {
                    allCardsChosen = false;
                    break;
                }
            }

            if (allCardsChosen == true)
            {
                completionProgramming.TrySetResult(info);
            }
            else
            {
                for (int i = 0; i < Constants.selectedCardsNum; i++)
                {
                    SelectedCards[i].Card = new CardModel(Constants.programmingCardBack);
                }
                completionProgramming.TrySetResult(new CardInfo[Constants.selectedCardsNum]);
            }
        }

        private void finishBuying()
        {
            completionPurchaseResponse.TrySetResult(new PurchaseResponse());
        }

        private void finishChoice()
        {
            completionChoice.TrySetResult(null);
        }

        private void finishChoiceless()
        {
            completionChoiceless.TrySetResult(false);
        }

        private void finishDiscard()
        {
            completionDiscard.TrySetResult(null);
            completionPurchaseResponse.TrySetResult(new PurchaseResponse());
        }

        private void finishChooseOne()
        {
            completionProgramming.TrySetResult(new CardInfo[1] { new CardInfo() { CardId = ReceivedCards[0].Card.CardId } });
        }        
    }
}
