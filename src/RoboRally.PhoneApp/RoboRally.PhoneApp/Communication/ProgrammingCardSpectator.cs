﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Server.Interfaces;

namespace RoboRally.PhoneApp.Communication
{
    /// <summary>
    /// This spectator informs us about various events concerning the programming cards
    /// Used only to highlight played registers
    /// </summary>
    class ProgrammingCardSpectator : IProgrammingCardSpectator
    {
        public void CardsDealt()
        {
        }

        public void CardsSelected(RobotInfo robot, int count)
        {
        }

        public void CardSelectionOver()
        {
        }

        /// <summary>
        /// Used to highlight which cards have been played
        /// </summary>
        /// <param name="register"></param>
        public void RegisterActivated(int register)
        {
            PlayerModel.Instance.activationViewModel.Registers[register - 1].Color = Xamarin.Forms.Color.Green;
        }

        public void CardRevealed(RevealedProgrammingCard revealedCard)
        {
        }
        
        public void CardBeingPlayed(RobotInfo robot, string card)
        {
        }

        public void DamageCardsTaken(RobotInfo robot, Damage damage)
        {
        }

        public void RevealedCardReplaced(RobotInfo robot, int register, CardInfo oldCard, CardInfo newCard)
        {
        }

        public void CardBurned(RobotInfo robot, string card)
        {
        }

        public void CardsNotSelectedOnTime(RobotInfo robot)
        {
        }

        public void GameStarted(IGame game)
        {
        }
    }
}
