﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Linq;

namespace RoboRally.PhoneApp.Communication
{
    /// <summary>
    /// This spectator informs us about events concerning upgrade cards
    /// Used for buying and using an upgrade
    /// </summary>
    class UpgradeCardSpectator : IUpgradeCardSpectator
    {
        public void CardsOfferedInShop(RobotInfo robot, PurchaseOffer offer)
        {
        }

        /// <summary>
        /// Notification from the server, that a robot bought a card
        /// If this is our robot, we add the card to the player
        /// </summary>
        /// <param name="robot">Robot buying the upgrade</param>
        /// <param name="boughtCard">Upgrade card bougt</param>
        public void CardBought(RobotInfo robot, PurchaseResponse boughtCard)
        {
            if (PlayerModel.Instance.robot != robot)
            {
                return;
            }

            if (boughtCard.SelectedCard.CardType == UpgradeCardType.Permanent)
            {
                if (boughtCard.DiscardedCard != null)
                {
                    UpgradeCardInfo target = PlayerModel.Instance.PermanentUpgrades.Single(i => i.CardId == boughtCard.DiscardedCard.CardId);
                    PlayerModel.Instance.PermanentUpgrades.Remove(target);
                }
                PlayerModel.Instance.PermanentUpgrades.Add(boughtCard.SelectedCard);
            }
            else
            {
                if (boughtCard.DiscardedCard != null)
                {
                    UpgradeCardInfo target = PlayerModel.Instance.TemporaryUpgrades.Single(i => i.CardId == boughtCard.DiscardedCard.CardId);
                    PlayerModel.Instance.TemporaryUpgrades.Remove(target);
                }
                PlayerModel.Instance.TemporaryUpgrades.Add(boughtCard.SelectedCard);
            }
        }

        public void NoCardBought(RobotInfo robot)
        {
        }

        public void UpgradeCardOffered(RobotInfo robot, CardInfo cardInfo)
        {
        }

        /// <summary>
        /// Notification from the server, that a given upgrade card has been used by a robot.
        /// We detect only the temporary cards.
        /// If an temporary card is used, we remove it from the player
        /// </summary>
        /// <param name="robot">Robot using the upgrade</param>
        /// <param name="cardSelection">Upgrade card used</param>
        public void UpgradeCardUsed(RobotInfo robot, CardInfo cardSelection)
        {
            try
            {
                if (PlayerModel.Instance.activationViewModel != null && PlayerModel.Instance.activation==true)
                {
                    PlayerModel.Instance.activationViewModel.DisableCard(cardSelection.CardId);
                }
                UpgradeCardInfo target = PlayerModel.Instance.TemporaryUpgrades.Single(i => i.CardId == cardSelection.CardId);
                PlayerModel.Instance.TemporaryUpgrades.Remove(target);
            }
            catch (Exception)
            {
            }
        }

        public void UpgradeCardNotUsed(RobotInfo robot)
        {
        }

        public void GameStarted(IGame game)
        {
        }
    }
}
