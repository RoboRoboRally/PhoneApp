﻿using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRally.PhoneApp.Views;
using RoboRoboRally.Model.CardDB.Cards;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Communication
{
    /// <summary>
    /// Through this spectator the server communicates various actions regargin gameplay
    /// These actions include programming cards selection, buying upgrade cards and more
    /// </summary>
    class PlayerClientSpectator : IPlayerClient
    {
        private async Task<CardInfo[]> WaitCardInfoArrayTask(TaskCompletionSource<CardInfo[]> tcs)
        {
            return await tcs.Task;
        }

        private async Task<PurchaseResponse> WaitPurchaseResponseTask(TaskCompletionSource<PurchaseResponse> tcs)
        {
            return await tcs.Task;
        }

        private async Task<bool> WaitBoolTask(TaskCompletionSource<bool> tcs)
        {
            return await tcs.Task;
        }

        private async Task<CardInfo> WaitCardInfoTask(TaskCompletionSource<CardInfo> tcs)
        {
            return await tcs.Task;
        }

        /// <summary>
        /// Called at the start of programming phase or when special card is used.
        /// Spawn CardsProgrammingPage
        /// </summary>
        /// <param name="count">Number of cards to select</param>
        /// <param name="cards">Collection of cards</param>
        /// <param name="timeout">Time to select</param>
        /// <returns></returns>
        public Task<CardInfo[]> ChooseProgrammingCards(int count, CardInfo[] cards, TimeSpan timeout)
        {
            PlayerModel.Instance.Init(timeout);
            for (int i = 0; i < cards.Length; i++)
            {
                PlayerModel.Instance.ReceivedCards.Add(new SlotModel(Controller.backgroundColor, new Models.CardModel(cards[i].CardId)));
            }
            PlayerModel.Instance.completionProgramming = new TaskCompletionSource<CardInfo[]>();

            if (count == 1)
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Controller.CallPage(new ChooseOnePage(PlayerModel.Instance.completionProgramming));
                });
            }
            else
            {
                Device.BeginInvokeOnMainThread(() =>
                {
                    Controller.CallPage(new CardsProgrammingPage(PlayerModel.Instance.completionProgramming));
                });
            }

            return WaitCardInfoArrayTask(PlayerModel.Instance.completionProgramming);
        }

        /// <summary>
        /// Called at the start of buy phase.
        /// Spawn BuyPage
        /// </summary>
        /// <param name="offer">Offered upgrade cards to buy</param>
        /// <param name="timeout">Time to buy</param>
        /// <returns></returns>
        public Task<PurchaseResponse> BuyUpgradeCard(PurchaseOffer offer, TimeSpan timeout)
        {
            PlayerModel.Instance.Init(timeout);
            PlayerModel.Instance.OwnedCardLimit = offer.OwnedCardLimit;
            PlayerModel.Instance.completionPurchaseResponse = new TaskCompletionSource<PurchaseResponse>();

            Device.BeginInvokeOnMainThread(() =>
            {
                Controller.CallPage(new BuyPage(PlayerModel.Instance.completionPurchaseResponse, offer));
            });

            return WaitPurchaseResponseTask(PlayerModel.Instance.completionPurchaseResponse);
        }

        /// <summary>
        /// Called when player owns an upgrade and the model needs to know, if we want to use it
        /// </summary>
        /// <param name="card">Card in question</param>
        /// <param name="timeout">Time to decide</param>
        /// <returns></returns>
        public Task<bool> OfferChoicelessUpgradeCard(CardInfo card, TimeSpan timeout)
        {
            PlayerModel.Instance.Init(timeout);
            PlayerModel.Instance.completionChoiceless = new TaskCompletionSource<bool>();
            Device.BeginInvokeOnMainThread(() =>
            {
                Controller.CallPage(new ChoicelessPage(PlayerModel.Instance.completionChoiceless, card));
            });

            return WaitBoolTask(PlayerModel.Instance.completionChoiceless);

        }

        /// <summary>
        /// Called when we own an upgrade with multiple choices and the server needs to know, which choise we want to use
        /// </summary>
        /// <param name="card">Card with choices</param>
        /// <param name="timeout">Time to decide</param>
        /// <returns></returns>
        public Task<CardInfo> OfferUpgradeCardWithChoices(UpgradeCardInfo card, TimeSpan timeout)
        {
            PlayerModel.Instance.Init(timeout);
            PlayerModel.Instance.completionChoice = new TaskCompletionSource<CardInfo>();
            Device.BeginInvokeOnMainThread(() =>
            {
                Controller.CallPage(new ChoicePage(PlayerModel.Instance.completionChoice, card));
            });

            return WaitCardInfoTask(PlayerModel.Instance.completionChoice);
        }

        /// <summary>
        /// Called during the activation phase, when card becomes available to play.
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public Task CardAvailableForPlay(UpgradeCardInfo card)
        {
            try
            {
                UpgradeCardInfo upgrade = PlayerModel.Instance.TemporaryUpgrades.Single(i => i.CardId == card.CardId);
                upgrade.Choices = card.Choices;

                SlotModel target = PlayerModel.Instance.activationViewModel.UpgradeCards.Single(i => i.Card.CardId == card.CardId);

                if (target != null)
                {
                    target.availabe = true;
                    target.Opacity = 1;
                    PlayerModel.Instance.activationViewModel.NextClicked();
                }
            }
            catch { }

            return Task.CompletedTask;
        }

        /// <summary>
        /// Called during activation phase, when the card is no longer available to play (because of reboot for example).
        /// </summary>
        /// <param name="card"></param>
        /// <returns></returns>
        public Task CardNoLongerAvailableForPlay(CardInfo card)
        {
            try
            {
                SlotModel target = PlayerModel.Instance.activationViewModel.UpgradeCards.Single(i => i.Card.CardId == card.CardId);

                if (target != null)
                {
                    target.availabe = false;
                    target.Opacity = Constants.dimOpacity;
                    PlayerModel.Instance.activationViewModel.NextClicked();
                }
            }
            catch { }

            return Task.CompletedTask;
        }

        public Task BatteryCountChanged(int newBatteryCount)
        {
            PlayerModel.Instance.Batteries = newBatteryCount;
            return Task.CompletedTask;
        }

        public async Task ProgrammingCardTimeout()
        {            
        }

        /// <summary>
        /// Called after connecting to the game. Is used to obtain state of robot for example after disconnecting
        /// </summary>
        /// <param name="robotState"></param>
        /// <returns></returns>
        public Task StateInitialisation(RobotState robotState)
        {
            PlayerModel.Instance.Batteries = robotState.Energy;
            PlayerModel.Instance.Checkpoints = robotState.LastPassedCheckpoint;
            PlayerModel.Instance.robot = robotState.Robot;

            try
            {
                foreach (UpgradeCardInfo upgrade in robotState.UpgradeCards[UpgradeCardType.Permanent])
                {
                    PlayerModel.Instance.PermanentUpgrades.Add(upgrade);
                }
            }
            catch { }

            try
            {
                foreach (UpgradeCardInfo upgrade in robotState.UpgradeCards[UpgradeCardType.Temporary])
                {
                    PlayerModel.Instance.TemporaryUpgrades.Add(upgrade);
                }
            }
            catch { }

            return Task.CompletedTask;
        }

        public void GameStarted(IGame game)
        {
            if (PlayerModel.Instance.game == null)
            {
                PlayerModel.Instance.game = game;
                PlayerModel.Instance.Clean();
                PlayerModel.Instance.Erase();
                Thread.Sleep(1000);
            }
        }
    }
}
