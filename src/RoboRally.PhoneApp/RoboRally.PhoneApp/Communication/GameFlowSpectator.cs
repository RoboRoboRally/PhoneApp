﻿using RoboRally.PhoneApp.Models;
using RoboRally.PhoneApp.ViewModels;
using RoboRally.PhoneApp.Views;
using RoboRoboRally.Server.Interfaces;
using System.Threading;
using Xamarin.Forms;

namespace RoboRally.PhoneApp.Communication
{
    /// <summary>
    /// Spectator which informs us about the game events
    /// Used for phases and the game over / game win events
    /// </summary>
    class GameFlowSpectator : IGameFlowSpectator
    {
        public void GameStarted(IGame game)
        {
        }

        public void TurnStarted(int turnNumber)
        {

        }

        public void TurnEnded(int turnNumber)
        {

        }

        /// <summary>
        /// We react if the phase is ActivationPhase - in that case spawn ActivationPage
        /// </summary>
        /// <param name="phaseName"></param>
        public void PhaseStarted(string phaseName)
        {
            if (phaseName == "ActivationPhase" && PlayerModel.Instance.action != PlayerModel.ActionType.None && PlayerModel.Instance.SelectedCards.Count == Constants.selectedCardsNum)
            {
                // If other pages need to be poped or pushed
                Thread.Sleep(1000);
                PlayerModel.Instance.activation = true;
                Device.BeginInvokeOnMainThread(() =>
                {
                    Controller.CallPage(new ActivationPage());
                });
                // To have time to push the page
                Thread.Sleep(1000);
            }
        }

        /// <summary>
        /// We react if the phase is ActivationPhase - in that case remove ActivationPage
        /// </summary>
        /// <param name="phaseName"></param>
        public void PhaseEnded(string phaseName)
        {
            if (phaseName == "ActivationPhase" && PlayerModel.Instance.action != PlayerModel.ActionType.None)
            {
                PlayerModel.Instance.activation = false;
                Controller.PopPages();
            }
        }

        public void GamePaused()
        {
        }

        public void GameResumed()
        {
        }

        /// <summary>
        /// Spawn the GameOverPage
        /// </summary> 
        public void GameOver()
        {
            PlayerModel.Instance.GameEnded();
            // If other pages need to be pushed or poped
            Thread.Sleep(1000);
            Device.BeginInvokeOnMainThread(() =>
            {
                Controller.CallPage(new GameOverPage());
            });
            // To have time for the page to push
            Thread.Sleep(1000);
        }

        public void GameIsEnding()
        {
        }
        /// <summary>
        /// If the robot is ours spawn the FinishedPage
        /// </summary>
        /// <param name="robot"></param>
        public void RobotFinishedGame(RobotInfo robot)
        {
            if (PlayerModel.Instance.robot == robot)
            {
                PlayerModel.Instance.GameEnded();
                // If other pages need to be pushed or poped
                Thread.Sleep(1000);
                Device.BeginInvokeOnMainThread(() =>
                {
                    Controller.CallPage(new FinishedPage());
                });
                // To have time to push the page
                Thread.Sleep(1000);
            }
        }

        public void RobotVisitedCheckpoint(RobotInfo robot, int checkpointNr)
        {
            if (PlayerModel.Instance.robot != robot)
            {
                return;
            }

            if (PlayerModel.Instance.Checkpoints + 1 == checkpointNr)
            {
                PlayerModel.Instance.Checkpoints = checkpointNr;
            }
        }

        public void RobotPriorityDetermined(RobotInfo[] robotsInOrder)
        {

        }
    }
}
