﻿namespace RoboRally.PhoneApp.Communication
{
    /// <summary>
    /// Class used to extract information from JSON using Newtonsoft
    /// </summary>
    class ConnectionData
    {
        /// <summary>
        /// Scanned ipv4 of the server
        /// </summary>
        public string Ip { get; set; }
        /// <summary>
        /// Scanned port on which the server listens
        /// </summary>
        public int Port { get; set; }
        /// <summary>
        /// Scanned lobby which we are connecting to
        /// </summary>
        public string LobbyName { get; set; }
        /// <summary>
        /// Scanned robot which we are using
        /// </summary>
        public string RobotName { get; set; }
    }
}
