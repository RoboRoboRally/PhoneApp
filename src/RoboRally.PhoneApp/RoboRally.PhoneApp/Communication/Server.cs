﻿using RoboRally.PhoneApp.Models;
using RoboRoboRally.Communications;
using RoboRoboRally.Server.Interfaces;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace RoboRally.PhoneApp.Communication
{
    static class Server
    {
        static IRpcConnection connection = null;

        /// <summary>
        /// Create used Spectators and register them. Then connect with the same interfaces
        /// Join lobby and select the robot from QR code
        /// </summary>
        /// <param name="uri">Server ipv4 adress combined with port from QR code</param>
        /// <param name="lobbyName">Name of the lobby from QR code</param>
        /// <param name="PlayerName">Name of the robot from QR code</param>
        public static void connect(Uri uri, string lobbyName, string PlayerName)
        {
            TaskCompletionSource<IRpcConnection> tcs = new TaskCompletionSource<IRpcConnection>();
            Task.Run(() => new RpcConnection(uri, s =>
            {
                tcs.TrySetResult(s);

            }));

            connection = tcs.Task.Result;
            connection.Register<IPlayerClient>(new PlayerClientSpectator());
            connection.Register<IGameFlowSpectator>(new GameFlowSpectator());
            connection.Register<IProgrammingCardSpectator>(new ProgrammingCardSpectator());
            connection.Register<IUpgradeCardSpectator>(new UpgradeCardSpectator());
            var server = connection.GetRemote<IServer>();
            server.Connect("Mobile App", new[] { typeof(IPlayerClient), typeof(IGameFlowSpectator), typeof(IProgrammingCardSpectator), typeof(IUpgradeCardSpectator) });
            var lobby = server.JoinLobby(lobbyName);
            RobotInfo[] robots = lobby.GetAvailableRobots();

            RobotInfo robot = robots.Single(i => i == PlayerName);
            lobby.ChangeMyRoleToPlayer(robot);
            PlayerModel.Instance.Color = robot.Color;
            PlayerModel.Instance.robot = robot;
        }

        public static async void disconnect()
        {
            if (connection != null)
            {
                try
                {
                    await Task.Run(() => connection.Dispose());
                }
                catch (Exception)
                {

                }
            }
        }
    }
}
