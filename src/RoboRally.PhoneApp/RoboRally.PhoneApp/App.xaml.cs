﻿using System.Globalization;
using System.Runtime.CompilerServices;

using Xamarin.Forms;

namespace RoboRally.PhoneApp
{
    [CompilerGenerated]
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            //Detect which language does the phone use and choose translation accordingly                        
            if (CultureInfo.CurrentCulture.TwoLetterISOLanguageName.ToString() == "cs")
            {
                AppResources.Culture = new CultureInfo("cs");
            }
            else
            {
                AppResources.Culture = new CultureInfo("en");
            }

            MainPage = new NavigationPage(new RoboRally.PhoneApp.MainPage());
        }

        protected override void OnStart()
        {
            // Handle when your app starts
        }

        protected override void OnSleep()
        {
            // Handle when your app sleeps
        }

        protected override void OnResume()
        {
            // Handle when your app resumes
        }
    }
}
